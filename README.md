# OpenML dataset: 1DUltrasoundMuscleContractionData

https://www.openml.org/d/41971

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This collection includes 21 data sets of one-dimensional ultrasound raw RF data (A-Scans) acquired from the calf muscles of 8 healthy volunteers. The subjects were asked to manually annotate the data to allow for muscle contraction classification tasks. Each line of the ARFF file contains an A-Scan consisting of 3000 amplitude values, an annotation (whether this A-Scan belongs to a contracted muscle (1) or not (0)), the ID of the subject and the ID of the data set.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41971) of an [OpenML dataset](https://www.openml.org/d/41971). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41971/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41971/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41971/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

